# README #


### What is this repository for? ###

* Quick summary

** API automation framework using RestAssured

* Project structure
	1. src/test/java/cloudleafApiautomation 
	we will add our test scripts in this directory
	
	2. Common
	We will have below 3 files
	a) Page.java
	This is base page which has be to extended by all test scripts. This contains setup, teardown methods, global config along with reports generation
	b) RestAPIUtility.java
	This is test object library which is specific to application. All the reusable methods will be added here.
	c) Utils.java
	This is generic utility for random number/random string generation, read config file etc.., This is not application specific.
	Hence, This can be used for any API automation code.
	
	3. test.java.resources
	We have config files and upload files for entity creation are placed in this directory.

* Version 
1.0


* Prerequisites

 	1. Eclipse
 	2. Java (latest version)
 	3. TestNG plugin for eclipse (http://dl.bintray.com/testng-team/testng-p2-release/)
 
### How do I get set up? ###

* Summary of set up

	1. Open eclipse
	2. Go to File --> import --> Maven --> Existing Maven projects
	3. Select project folder
	4. Click Finish


This will automatically install maven dependencies from pom.xml file.

* How to run tests
	1. Go to src/test/java/cloudleafApiautomation directory
	2. Right click on any file
	3. Select Run as TestNG test



### Contribution guidelines ###

* Writing tests
	1. Create a new Java file under src/test/java/cloudleafApiautomation that extends Page.java
		e.g. CreateEntity.java 
		public class CreateEntity extends Page 
		{
			// your code goes here
		}

*Viewing reports
	
	
	***We have extent reports configured in this framework.
	1. Go to ExtentReports directory
	2. Open Report with latest timestamp