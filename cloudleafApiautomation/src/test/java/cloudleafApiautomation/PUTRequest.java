package cloudleafApiautomation;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import common.APIBasePage;
import common.RestAPIUtility;
import common.Utils;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class PUTRequest extends APIBasePage {

	@Test(enabled = true, priority = 1)
	public void test_Update_a_node() throws InterruptedException {
		test = extent.startTest("Update a node");

		String nodeID = Utils.createUUID();
		String name = "Trailer-" + Utils.generateRandomNumber();

		RestAssured.baseURI = api_url + "/vertex";

		response = RestAPIUtility.createNode(RestAssured.baseURI, nodeID, name);

		System.out.println("Response :" + response.asString());
		System.out.println("Status Code :" + response.getStatusCode());

		System.out.println("ID: " + RestAPIUtility.parseJson(response, "id"));

		assertEquals(response.getStatusCode(), 201);

		assertEquals(RestAPIUtility.parseJson(response, "id"), nodeID);

		RestAssured.baseURI = api_url + "/vertex/" + nodeID;

		String updatedName = "Trailer-" + Utils.generateRandomNumber();

		response = RestAPIUtility.UpdateNode(RestAssured.baseURI, updatedName);

		System.out.println("Response :" + response.asString());
		System.out.println("Status Code :" + response.getStatusCode());

		assertEquals(response.getStatusCode(), 200);

	}

	@Test(enabled = true, priority = 2)
	public void test_Update_RelationShip() throws InterruptedException {
		test = extent.startTest("Update RelationShip");

		RestAssured.baseURI = api_url + "/edge";

		Response response1, response2;

		// start of create node

		String firstNodeID = Utils.createUUID();
		String secondNodeID = Utils.createUUID();
		String firstName = "Trailer-" + Utils.generateRandomNumber();
		String secondName = "Trailer-" + Utils.generateRandomNumber();

		RestAssured.baseURI = api_url + "/vertex";

		response1 = RestAPIUtility.createNode(RestAssured.baseURI, firstNodeID, firstName);
		response2 = RestAPIUtility.createNode(RestAssured.baseURI, secondNodeID, secondName);

		System.out.println("Response :" + response1.asString());
		System.out.println("Status Code :" + response1.getStatusCode());

		System.out.println("Response :" + response2.asString());
		System.out.println("Status Code :" + response2.getStatusCode());

		assertEquals(201, response1.getStatusCode());

		assertEquals(RestAPIUtility.parseJson(response1, "id"), firstNodeID);

		assertEquals(RestAPIUtility.parseJson(response2, "id"), secondNodeID);

		// end of create node

		// start of create relationship

		response = RestAPIUtility.createRelationShip(RestAssured.baseURI, firstNodeID, secondNodeID);

		System.out.println("Create Relationship Response :" + response.asString());
		System.out.println("Status Code :" + response.getStatusCode());

		assertEquals(response.getStatusCode(), 201);

		assertEquals(RestAPIUtility.parseJson(response, "id1"), firstNodeID);
		assertEquals(RestAPIUtility.parseJson(response, "id2"), secondNodeID);
		assertEquals(RestAPIUtility.parseJson(response, "properties.from"), firstNodeID);
		assertEquals(RestAPIUtility.parseJson(response, "properties.to"), secondNodeID);

		// end of create relationship

		// start of update relationship

		String relationShip = firstNodeID + "_TO_" + secondNodeID;

		RestAssured.baseURI = api_url + "/edge/" + firstNodeID + "_TO_" + secondNodeID;

		String modifiedBy = "Nazim";

		response = RestAPIUtility.updateRelationShip(RestAssured.baseURI, modifiedBy);

		System.out.println("Update Relationship Response :" + response.asString());
		System.out.println("Status Code :" + response.getStatusCode());

		assertEquals(response.getStatusCode(), 200);

		jsonPathEvaluator = response.jsonPath();

		assertEquals(RestAPIUtility.parseJson(response, "id"), relationShip);
		assertEquals(RestAPIUtility.parseJson(response, "properties.from"), firstNodeID);
		assertEquals(RestAPIUtility.parseJson(response, "properties.to"), secondNodeID);

	}

}
