package cloudleafApiautomation;

import static org.testng.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import common.APIBasePage;
import common.Utils;
import io.restassured.RestAssured;

import io.restassured.http.ContentType;
import io.restassured.specification.MultiPartSpecification;
import io.restassured.builder.MultiPartSpecBuilder;

public class POSTRequest_FileUpload extends APIBasePage {

	@Test(enabled = true, priority = 6)
	public void test_Upload_Entities_with_CDM_type_as_input() throws InterruptedException, IOException {
		test = extent.startTest("Upload Entities with CDM type as input (Instances, e.g assets, trucks etc)");

		RestAssured.baseURI = api_url + "/bootstrap/entity?cdmType=Asset";

		File file = new File(Utils.ASSET_FILE);
		String fieldName = "file";

		byte[] fileContent = FileUtils.readFileToByteArray(file);

		try {

			response = RestAssured.given()
					.header("token", superadmin_token)
					.accept(ContentType.JSON)
		            .header("Content-type", "multipart/form-data")
					.multiPart(getMultiPart()).when().post();
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("API URL: " + RestAssured.baseURI);
		System.out.println("Response :" + response.asString());
		System.out.println("Status Code :" + response.getStatusCode());

		assertEquals(response.getStatusCode(), 201);

		test.log(LogStatus.PASS, "Upload Entities with CDM type as input");
	}

	@Test(enabled = true, priority = 6)
	public void test_Upload_Entities() throws InterruptedException, IOException {
		test = extent.startTest("Upload Entities with CDM type as input (Instances, e.g assets, trucks etc)");

		RestAssured.baseURI = api_url + "/bootstrap/entity?cdmType=Asset";

		File file = new File(Utils.ASSET_FILE);

		try {

			response = RestAssured.given().accept(ContentType.JSON).header("token", superadmin_token)
					.header("Content-Type", "multipart/form-data").multiPart("file", file, "multipart/form-data")
					.post();
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("API URL: " + RestAssured.baseURI);
		System.out.println("Response :" + response.asString());
		System.out.println("Status Code :" + response.getStatusCode());

		assertEquals(response.getStatusCode(), 201);

		test.log(LogStatus.PASS, "Upload Entities with CDM type as input");

		test.log(LogStatus.PASS, "Upload Entities with CDM type as input");
	}

	@Test(enabled = true, priority = 7)
	public void test_Upload_Entities_without_CDM_type_as_input() throws InterruptedException {
		test = extent.startTest("Upload Entities without CDM type as input (Instances, e.g assets, trucks etc)");

		RestAssured.baseURI = api_url + "/bootstrap/entity";

		File file = new File(Utils.ASSET_FILE);

		try {
			response = RestAssured.given().header("token", superadmin_token)
					.multiPart(new MultiPartSpecBuilder(file).with().fileName(Utils.ASSET_FILE).controlName("file")
							.and().with().mimeType("application/vnd.ms-excel").build())
					.post();
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Response :" + response.asString());
		System.out.println("Status Code :" + response.getStatusCode());

		assertEquals(response.getStatusCode(), 201);

		test.log(LogStatus.PASS, "Upload Entities with CDM type as input");
	}

	@Test(enabled = true, priority = 8)
	public void test_Bulk_upload_Entities_with_Multiple_CSVs() throws InterruptedException {
		test = extent.startTest("7. Bulk upload Entities with Multiple CSVs (Instances, e.g assets, trucks etc)");

		RestAssured.baseURI = api_url + "/vertex/all/bulk";

		try {
			response = RestAssured.given().contentType(ContentType.JSON).header("Accept", "application/json")
					.header("token", superadmin_token).post();
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Response :" + response.asString());
		System.out.println("Status Code :" + response.getStatusCode());

		assertEquals(response.getStatusCode(), 201);

		test.log(LogStatus.PASS, "Upload Entities with CDM type as input");
	}

	@Test(enabled = true, priority = 9)
	public void test_Upload_Relationships_with_CDM_Type_as_input() throws InterruptedException {
		test = extent.startTest(
				"Upload a Relationships with CDM Type as input (links or edges, e.g Hirarchies, Routes etc)");

		RestAssured.baseURI = api_url + "/bootstrap/relation?cdmType=Has";

		try {
			response = RestAssured.given().contentType(ContentType.JSON).header("Accept", "application/json")
					.header("token", superadmin_token).post();
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Response :" + response.asString());
		System.out.println("Status Code :" + response.getStatusCode());

		assertEquals(response.getStatusCode(), 201);

		test.log(LogStatus.PASS, "Upload Entities with CDM type as input");
	}

	@Test(enabled = true, priority = 10)
	public void test_Upload_Relationships_without_CDM_Type_as_input() throws InterruptedException {
		test = extent.startTest(
				"Upload a Relationships without CDM Type as input (links or edges, e.g Hirarchies, Routes etc)");

		RestAssured.baseURI = api_url + "/bootstrap/relation";

		try {
			response = RestAssured.given().contentType(ContentType.JSON).header("Accept", "application/json")
					.header("token", superadmin_token).post();
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Response :" + response.asString());
		System.out.println("Status Code :" + response.getStatusCode());

		assertEquals(response.getStatusCode(), 201);

		test.log(LogStatus.PASS, "Upload Entities with CDM type as input");
	}

	@Test(enabled = true, priority = 11)
	public void test_Bulk_upload_a_Relationships_with_Multiple_CSVs() throws InterruptedException {
		test = extent.startTest(
				"Bulk upload a Relationships with Multiple CSVs (links or edges, e.g Hirarchies, Routes etc)");

		RestAssured.baseURI = api_url + "/edge/all/bulk";

		try {
			response = RestAssured.given().contentType(ContentType.JSON).header("Accept", "application/json")
					.header("token", superadmin_token).post();
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Response :" + response.asString());
		System.out.println("Status Code :" + response.getStatusCode());

		assertEquals(response.getStatusCode(), 201);

		test.log(LogStatus.PASS, "Upload Entities with CDM type as input");
	}

	private MultiPartSpecification getMultiPart() throws IOException {
		File file = new File(Utils.ASSET_FILE);
		String fieldName = "file";

		byte[] fileContent = FileUtils.readFileToByteArray(file);
		return new MultiPartSpecBuilder(fileContent).fileName(Utils.ASSET_FILE).controlName("file")
				.mimeType("application/octet-stream").build();
	}

}
