package cloudleafApiautomation;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import common.APIBasePage;
import common.RestAPIUtility;
import common.Utils;
import io.restassured.RestAssured;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class POSTRequest extends APIBasePage {

	@Test(enabled = true, priority = 1)
	public void test_createTenant_Existing_TenantID() throws InterruptedException {
		test = extent.startTest("Create Tenant using existing tenant ID");
		RestAssured.baseURI = api_url + "/database/" + tenant_id;

		response = RestAPIUtility.createPostRequest(RestAssured.baseURI, requestBody);

		System.out.println("Response :" + response.asString());
		System.out.println("Status Code :" + response.getStatusCode());

		assertEquals(response.getStatusCode(), 409);
		assertEquals("Already Exists", response.asString());

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());
		test.log(LogStatus.INFO, "Response :" + response.asString());
	}

	@Test(enabled = true, priority = 2)
	public void test_createNewTenant() throws InterruptedException {

			test = extent.startTest("Create Tenant using new tenant ID ");
			RestAssured.baseURI = api_url + "/database/" + Utils.createUUID();

			response = RestAPIUtility.createPostRequest(RestAssured.baseURI, requestBody);

			System.out.println("Response :" + response.asString());
			System.out.println("Status Code :" + response.getStatusCode());

			assertEquals(response.getStatusCode(), 201);
			assertEquals("Created", response.asString());

			test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());
			test.log(LogStatus.INFO, "Response :" + response.asString());

			response = RestAPIUtility.createDeleteRequest(RestAssured.baseURI);
			System.out.println("Response :" + response.asString());
			System.out.println("Status Code :" + response.getStatusCode());
	}

	@Test(enabled = true, priority = 3)
	public void test_Create_Entity_Type() throws InterruptedException {
		test = extent.startTest("Create an Entity Type (UserDefined)");

		RestAssured.baseURI = api_url + "/cdm/ToolKit?superClass=Asset";

		String ID = Utils.generateRandomNumber();
		String entityID = "ToolKit" + ID, entityName = "ToolKit" + ID, cdmID = "toolShape" + ID,
				cdmName = "toolShape" + ID;

		response = RestAPIUtility.createEntityType(RestAssured.baseURI, entityID, entityName, cdmID, cdmName);

		System.out.println("Response :" + response.asString());
		System.out.println("Status Code :" + response.getStatusCode());

		assertEquals(response.getStatusCode(), 201);

		System.out.println("type: " + RestAPIUtility.parseJson(response, "type"));
		System.out.println("graphType: " + RestAPIUtility.parseJson(response, "properties.graphType"));

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());
		test.log(LogStatus.INFO, "Response :" + response.asString());

	}

	@Test(enabled = true, priority = 4)
	public void test_Mapping_for_Entity() throws InterruptedException {
		test = extent.startTest("Create mapping for Entity");

		RestAssured.baseURI = api_url + "/mapping/" + tenant_id + "/asset";

		response = RestAPIUtility.createMappingForEntity(RestAssured.baseURI);

		try {
			assertEquals(response.getStatusCode(), 201);
			test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());
			test.log(LogStatus.INFO, "Response :" + response.asString());
			test.log(LogStatus.PASS, "Create an Entity Type (UserDefined)");
		} catch (AssertionError ae) {
			test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());
			test.log(LogStatus.INFO, "Response :" + response.asString());
			test.log(LogStatus.FAIL, "Create an Entity Type (UserDefined)");
		}

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());
		test.log(LogStatus.INFO, "Response :" + response.asString());

	}

	@Test(enabled = true, priority = 5)
	public void test_Create_Bulk_Mapping_for_all_Entity_types_for_a_given_tenant() throws InterruptedException {
		test = extent.startTest("Create Bulk Mapping for all Entity types for a given tenant");

		RestAssured.baseURI = api_url + "/mapping/bulk/" + tenant_id;

		response = RestAPIUtility.createBulkMappingForAllEntityTypesForaGivenTenant(RestAssured.baseURI);

		System.out.println("Response :" + response.asString());
		System.out.println("Status Code :" + response.getStatusCode());

		assertEquals(response.getStatusCode(), 201);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());
		test.log(LogStatus.INFO, "Response :" + response.asString());
	}

	@Test(enabled = true, priority = 11)
	public void test_Bulk_upload_a_Relationships_with_Multiple_CSVs() throws InterruptedException {
		test = extent.startTest(
				"Bulk upload a Relationships with Multiple CSVs (links or edges, e.g Hirarchies, Routes etc)");

		RestAssured.baseURI = api_url + "/edge/all/bulk";

		try {
			response = RestAssured.given().contentType(ContentType.JSON).header("Accept", "application/json")
					.header("token", superadmin_token).post();
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Response :" + response.asString());
		System.out.println("Status Code :" + response.getStatusCode());

		assertEquals(response.getStatusCode(), 201);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());
		test.log(LogStatus.INFO, "Response :" + response.asString());
	}

	@Test(enabled = true, priority = 12)
	public void test_Create_node() throws InterruptedException {
		test = extent.startTest("Create a node");

		RestAssured.baseURI = api_url + "/vertex";

		String nodeID = Utils.createUUID();
		String name = "Trailer-" + Utils.generateRandomNumber();

		response = RestAPIUtility.createNode(RestAssured.baseURI, nodeID, name);

		System.out.println("Response :" + response.asString());
		System.out.println("Status Code :" + response.getStatusCode());

		assertEquals(response.getStatusCode(), 201);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());
		test.log(LogStatus.INFO, "Response :" + response.asString());
	}

	@Test(enabled = true, priority = 13)
	public void test_Create_Relationship() throws InterruptedException {
		test = extent.startTest("Create a Relationship");

		
		RestAssured.baseURI = api_url + "/vertex";
		String firstNodeID = Utils.createUUID();
		String secondNodeID = Utils.createUUID();
		
		String firstName = "Trailer-" + Utils.generateRandomNumber();
		String secondName = "Trailer-" + Utils.generateRandomNumber();
		
		Response response1 = RestAPIUtility.createNode(RestAssured.baseURI, firstNodeID, firstName);
		Response response2 = RestAPIUtility.createNode(RestAssured.baseURI, secondNodeID, secondName);
		
		System.out.println("Status Code 1:" + response1.getStatusCode());
		System.out.println("Response 1:" + response1.asString());
		test.log(LogStatus.INFO, "Status Code 1:" + response1.getStatusCode());
		test.log(LogStatus.INFO, "Response 1:" + response1.asString());
		
		test.log(LogStatus.INFO, "Status Code 2:" + response2.getStatusCode());
		test.log(LogStatus.INFO, "Response 2:" + response2.asString());
		
		RestAssured.baseURI = api_url + "/edge";
		response = RestAPIUtility.createRelationShip(RestAssured.baseURI, firstNodeID, secondNodeID);

		System.out.println("Create Relationship Response :" + response.asString());
		System.out.println("Status Code :" + response.getStatusCode());

		test.log(LogStatus.INFO, "Status Code 1:" + response1.getStatusCode());
		test.log(LogStatus.INFO, "Response 1:" + response1.asString());
		
		test.log(LogStatus.INFO, "Status Code 2:" + response2.getStatusCode());
		test.log(LogStatus.INFO, "Response 2:" + response2.asString());
		
		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());
		test.log(LogStatus.INFO, "Response :" + response.asString());

		assertEquals(response.getStatusCode(), 201);

		assertEquals(RestAPIUtility.parseJson(response, "id1"), firstNodeID);
		assertEquals(RestAPIUtility.parseJson(response, "id2"), secondNodeID);
		assertEquals(RestAPIUtility.parseJson(response, "properties.from"), firstNodeID);
		assertEquals(RestAPIUtility.parseJson(response, "properties.to"), secondNodeID);

	}

	@Test(enabled = true, priority = 13)
	public void test_Create_RuleTemplate() throws InterruptedException {
		test = extent.startTest("Create a RuleTemplate");

		RestAssured.baseURI = api_url + "/rule/template";

		String name = "RuleTemplate_" + Utils.generateRandomNumber();
		String label = "RuleTemplate_" + Utils.generateRandomNumber() + "_T";
		String nodeID = Utils.createUUID();

		response = RestAPIUtility.createRuleTemplate(RestAssured.baseURI, name, label, nodeID);

		System.out.println("Response :" + response.asString());
		System.out.println("Status Code :" + response.getStatusCode());

		assertEquals(response.getStatusCode(), 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());
		test.log(LogStatus.INFO, "Response :" + response.asString());
	}

	@Test(enabled = true, priority = 14)
	public void test_Create_a_Complex_Rule_Actions_to_RuleNode() throws InterruptedException {
		test = extent.startTest("Create a Complex Rule, Actions to RuleNode");

		RestAssured.baseURI = api_url + "/rule";

		response = RestAPIUtility.createComplexRuleActionsToRuleNode(RestAssured.baseURI);

		System.out.println("Response :" + response.asString());
		System.out.println("Status Code :" + response.getStatusCode());

		assertEquals(response.getStatusCode(), 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());
		test.log(LogStatus.INFO, "Response :" + response.asString());
	}

	@Test(enabled = true, priority = 15)
	public void test_Create_a_Complex_Rule_Tailed_Actions() throws InterruptedException {
		test = extent.startTest("Create a Simple Rule e.g. Location Entry");

		RestAssured.baseURI = api_url + "/rule";

		response = RestAPIUtility.createComplexRuleTailedActions(RestAssured.baseURI);

		System.out.println("Response :" + response.asString());
		System.out.println("Status Code :" + response.getStatusCode());

		assertEquals(response.getStatusCode(), 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());
		test.log(LogStatus.INFO, "Response :" + response.asString());
	}

	@Test(enabled = true, priority = 16)
	public void test_Create_a_Simple_Rule_Location_Entry() throws InterruptedException {
		test = extent.startTest("Create a Simple Rule e.g. Location Entry");

		RestAssured.baseURI = api_url + "/rule";

		response = RestAPIUtility.createSimpleRuleLocationEntry(RestAssured.baseURI);

		System.out.println("Response :" + response.asString());
		System.out.println("Status Code :" + response.getStatusCode());

		assertEquals(response.getStatusCode(), 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());
		test.log(LogStatus.INFO, "Response :" + response.asString());
	}

	@Test(enabled = true, priority = 17)
	public void test_Create_a_Simple_Rule_Location_Dwell_Time() throws InterruptedException {
		test = extent.startTest("Create a Simple Rule e.g. Location Dwell Time");

		RestAssured.baseURI = api_url + "/rule";

		response = RestAPIUtility.CreateSimpleRuleLocationDwellTime(RestAssured.baseURI);

		System.out.println("Response :" + response.asString());
		System.out.println("Status Code :" + response.getStatusCode());

		assertEquals(response.getStatusCode(), 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());
		test.log(LogStatus.INFO, "Response :" + response.asString());
	}

}
