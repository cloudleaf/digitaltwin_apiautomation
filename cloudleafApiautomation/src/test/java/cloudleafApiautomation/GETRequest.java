package cloudleafApiautomation;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import common.APIBasePage;
import common.RestAPIUtility;
import io.restassured.RestAssured;
import io.restassured.response.Response;

import java.io.FileNotFoundException;

public class GETRequest extends APIBasePage {

	@Test(enabled = true, priority = 1)
	public void test_Get_All_Entity_Types() throws FileNotFoundException {
		test = extent.startTest("Get All Entity Types");

		RestAssured.baseURI = api_url + "/cdm/classtype/all?addFields=false&metaData=true";

		response = RestAPIUtility.createGETRequest(RestAssured.baseURI);

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.getBody().asString());
		Assert.assertEquals(code, 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

	@Test(enabled = true, priority = 2)
	public void test_Get_Relationship_Types() throws FileNotFoundException {
		test = extent.startTest("Get Relationship Types");

		RestAssured.baseURI = api_url + "/cdm/classtype/entity/all?addFields=true&graphType=Edge&metaData=true";

		response = RestAPIUtility.createGETRequest(RestAssured.baseURI);

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.getBody().asString());
		Assert.assertEquals(code, 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

	@Test(enabled = true, priority = 3)
	public void test_Get_All_Entities() throws FileNotFoundException {
		test = extent.startTest("Get all Entities");

		RestAssured.baseURI = api_url + "/cdm/classtype/entity/all?addFields=true&graphType=Node&metaData=true";

		response = RestAPIUtility.createGETRequest(RestAssured.baseURI);

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.getBody().asString());
		Assert.assertEquals(code, 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

	@Test(enabled = true, priority = 4)
	public void test_Visualize_Get_All_Graph_data_in_Keylines_format() throws FileNotFoundException {
		test = extent.startTest("Visualize (Get All Graph data in Keylines format)");

		RestAssured.baseURI = api_url + "/keylines/linkchart/all";

		response = RestAPIUtility.createGETRequest(RestAssured.baseURI);

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.getBody().asString());
		Assert.assertEquals(code, 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

	@Test(enabled = true, priority = 5)
	public void test_Visualize_Get_Graph_Data_by_Entity_Class() throws FileNotFoundException {
		test = extent.startTest("Visualize (Get Graph Data by Entity Class)");

		RestAssured.baseURI = api_url + "/keylines/linkchart/traverse?type=Asset,Incident,StateNode";

		response = RestAPIUtility.createGETRequest(RestAssured.baseURI);

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.getBody().asString());
		Assert.assertEquals(code, 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

	@Test(enabled = true, priority = 6)
	public void test_Visualize_Get_Graph_Data_by_id() throws FileNotFoundException {
		test = extent.startTest("Visualize (Get Graph Data by id)");

		RestAssured.baseURI = api_url
				+ "/keylines/linkchart/traverse?id=8484c083-c490-4def-b2cb-dc774c135b19&depth=1&direction=both";

		response = RestAPIUtility.createGETRequest(RestAssured.baseURI);

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.getBody().asString());
		Assert.assertEquals(code, 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

	@Test(enabled = true, priority = 7)
	public void test_Visualize_Get_Graph_Data_by_domain_object_copy() throws FileNotFoundException {
		test = extent.startTest("Visualize (Get Graph Data by  domain object) copy");

		RestAssured.baseURI = api_url + "/keylines/linkchart/traverse/domain?";

		Response response = RestAssured.given().header("Accept", "*/*").header("token", superadmin_token)
				.param("domain", "SC Physical").get();

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.getBody().asString());
		Assert.assertEquals(code, 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

	@Test(enabled = true, priority = 8)
	public void test_Visualize_Menu_DT() throws FileNotFoundException {
		test = extent.startTest("Visualize Menu DT");

		RestAssured.baseURI = api_url
				+ "/keylines/linkchart/analysis/menu/dt?dbname=c858f6b6-82b4-4d0b-af75-dad76a64743c";

		response = RestAPIUtility.createGETRequest(RestAssured.baseURI);

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.getBody().asString());
		Assert.assertEquals(code, 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

	@Test(enabled = true, priority = 9)
	public void test_Visualize_Analysis_Query() throws FileNotFoundException {
		test = extent.startTest("Visualize Analysis query");

		RestAssured.baseURI = api_url + "/keylines/linkchart/analysis/query/data?";

		Response response = RestAssured.given().header("Accept", "*/*").header("token", superadmin_token)
				.param("query", "select from shipment").get();

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.getBody().asString());
		Assert.assertEquals(code, 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

	@Test(enabled = true, priority = 10)
	public void test_Visualize_Analysis_Query_Classifier() throws FileNotFoundException {
		test = extent.startTest("Visualize Analysis Query classifier");

		RestAssured.baseURI = api_url + "/keylines/linkchart/analysis/query/data?";

		Response response = RestAssured.given().header("Accept", "*/*").header("token", superadmin_token)
				.param("query", "select from shipment").param("clasifier", "shipment_type")
				.param("clasifierValue", "internal").get();

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.getBody().asString());
		Assert.assertEquals(code, 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

	@Test(enabled = true, priority = 11)
	public void test_Visualize_Excursion_data() throws FileNotFoundException {
		test = extent.startTest("Visualize Excursion data");

		RestAssured.baseURI = api_url + "/keylines/linkchart/analysis/excursion/data";

		response = RestAPIUtility.createGETRequest(RestAssured.baseURI);

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.getBody().asString());
		Assert.assertEquals(code, 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

	@Test(enabled = true, priority = 12)
	public void test_Get_All_Metric_Types() throws FileNotFoundException {
		test = extent.startTest("Get All Metric Types (e.g Loation, Dwelltime, Count etc)");

		RestAssured.baseURI = api_url + "/cdm/classtype/metric/all?";

		Response response = RestAssured.given().header("Accept", "*/*").header("token", superadmin_token)
				.param("addFields", "true").param("metaData", "true").get();

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.getBody().asString());
		Assert.assertEquals(code, 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

	@Test(enabled = true, priority = 13)
	public void test_Get_Action_Types() throws FileNotFoundException {
		test = extent.startTest("Get Action Types (e.g Sms, Email, Web etc)");

		RestAssured.baseURI = api_url + "/cdm/classtype/action/all?";

		Response response = RestAssured.given().header("Accept", "*/*").header("token", superadmin_token)
				.param("addFields", "true").param("metaData", "true").get();

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.getBody().asString());
		Assert.assertEquals(code, 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

	@Test(enabled = true, priority = 14)
	public void test_Get_Details_of_Single_Node() throws FileNotFoundException {
		test = extent.startTest("Get details of a single node");

		RestAssured.baseURI = api_url + "/vertex/dfca0869-1e9f-4f26-b24e-eb5c865fea5a";

		response = RestAPIUtility.createGETRequest(RestAssured.baseURI);

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.getBody().asString());
		Assert.assertEquals(code, 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

	@Test(enabled = true, priority = 15)
	public void test_Get_All_Graph_Data_in_JSON_for_a_tenant() throws FileNotFoundException {
		test = extent.startTest("Get All Graph data in json for a tenant");

		RestAssured.baseURI = api_url + "/database/all";

		response = RestAPIUtility.createGETRequest(RestAssured.baseURI);

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.getBody().asString());
		Assert.assertEquals(code, 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

	@Test(enabled = true, priority = 16)
	public void test_Get_a_Node_By_ID() throws FileNotFoundException {
		test = extent.startTest("Get a node by ID");

		RestAssured.baseURI = api_url + "/vertex/97e2049a-84f4-4ce4-ab4e-a9e49f0f6eab";

		response = RestAPIUtility.createGETRequest(RestAssured.baseURI);

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.getBody().asString());
		Assert.assertEquals(code, 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

	@Test(enabled = true, priority = 17)
	public void test_Get_All_Node_by_Type() throws FileNotFoundException {
		test = extent.startTest("Get All node by type");

		RestAssured.baseURI = api_url + "/vertex/type/Asset";

		response = RestAPIUtility.createGETRequest(RestAssured.baseURI);

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.getBody().asString());
		Assert.assertEquals(code, 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

	@Test(enabled = true, priority = 18)
	public void test_Get_All_Relationship_by_Type() throws FileNotFoundException {
		test = extent.startTest("Get All Relationship by type");

		RestAssured.baseURI = api_url + "/edge/type/HasEdge";

		response = RestAPIUtility.createGETRequest(RestAssured.baseURI);

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.getBody().asString());
		Assert.assertEquals(code, 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

	@Test(enabled = true, priority = 19)
	public void test_Get_RelationShip_by_ID() throws FileNotFoundException {
		test = extent.startTest("Get RelationShip by ID");

		RestAssured.baseURI = api_url + "/edge/E06005D89E49_TO_97e2049a-84f4-4ce4-ab4e-a9e49f0f6eab";

		response = RestAPIUtility.createGETRequest(RestAssured.baseURI);

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.getBody().asString());
		Assert.assertEquals(code, 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

	@Test(enabled = true, priority = 20)
	public void test_Get_a_RuleTemplate() throws FileNotFoundException {
		test = extent.startTest("Get a Rule Template");

		RestAssured.baseURI = api_url + "/rule/template/286ed8ae-85c1-4211-813d-189aa115ecb6";

		response = RestAPIUtility.createGETRequest(RestAssured.baseURI);

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.getBody().asString());
		Assert.assertEquals(code, 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

	@Test(enabled = true, priority = 21)
	public void test_List_RuleTemplates() throws FileNotFoundException {
		test = extent.startTest("List Rule Templates");

		RestAssured.baseURI = api_url + "/rule/template/all";

		response = RestAPIUtility.createGETRequest(RestAssured.baseURI);

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.getBody().asString());
		Assert.assertEquals(code, 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

	@Test(enabled = true, priority = 22)
	public void test_List_Rules() throws FileNotFoundException {
		test = extent.startTest("List Rules");

		RestAssured.baseURI = api_url + "/rule/all";

		response = RestAPIUtility.createGETRequest(RestAssured.baseURI);

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.getBody().asString());
		Assert.assertEquals(code, 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

	@Test(enabled = true, priority = 23)
	public void test_Get_a_Rule() throws FileNotFoundException {
		test = extent.startTest("Get a Rule");

		RestAssured.baseURI = api_url + "/rule/250ea11b-0c7c-470c-b0ec-1d9cb8672121";

		response = RestAPIUtility.createGETRequest(RestAssured.baseURI);

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.getBody().asString());
		Assert.assertEquals(code, 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

	@Test(enabled = true, priority = 24)
	public void test_Traverse_Graph_and_Get_Data() throws FileNotFoundException {
		test = extent.startTest("Traverse Graph and Get Data(Get Tree)");

		RestAssured.baseURI = api_url + "/traverse?";

		Response response = RestAssured.given().header("Accept", "*/*").header("token", superadmin_token)
				.param("type", "_Rule").param("id", "713d159a-e41b-4fa8-8fe9-10e0fac41427").param("depth", "2")
				.param("direction", "out").get();

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.getBody().asString());
		Assert.assertEquals(code, 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

	@Test(enabled = true, priority = 25)
	public void test_Traverse_Graph_and_Get_Keylines_Data() throws FileNotFoundException {
		test = extent.startTest("Traverse Graph and Get Keylines Data(Get Tree)");

		RestAssured.baseURI = api_url + "/keylines/linkchart/traverse?";

		Response response = RestAssured.given().header("Accept", "*/*").header("token", superadmin_token)
				.param("type", "_Rule").param("id", "713d159a-e41b-4fa8-8fe9-10e0fac41427").param("depth", "2")
				.param("direction", "out").get();

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.getBody().asString());
		Assert.assertEquals(code, 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

	@Test(enabled = true, priority = 26)
	public void test_Master_Data_Entity_List() throws FileNotFoundException {
		test = extent.startTest("Master Data Entity list");

		RestAssured.baseURI = api_url + "/cdm/classtype/entity/all?";

		Response response = RestAssured.given().header("Accept", "*/*").header("token", superadmin_token)
				.param("addFields", "false").param("graphType", "Node").param("metaData", "false").get();

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.getBody().asString());
		Assert.assertEquals(code, 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

	@Test(enabled = true, priority = 27)
	public void test_Master_Data_RelationShip_List() throws FileNotFoundException {
		test = extent.startTest("Master Data RelationShip list");

		RestAssured.baseURI = api_url + "/cdm/classtype/entity/all?";

		Response response = RestAssured.given().header("Accept", "*/*").header("token", superadmin_token)
				.param("addFields", "false").param("graphType", "Edge").param("metaData", "false").get();

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.getBody().asString());
		Assert.assertEquals(code, 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

	@Test(enabled = true, priority = 28)
	public void test_Virtualize_System_Entities() throws FileNotFoundException {
		test = extent.startTest("Virtualize system Entities");

		RestAssured.baseURI = api_url + "/cdm/classtype/entity/all?";

		Response response = RestAssured.given().header("Accept", "*/*").header("token", superadmin_token)
				.param("addFields", "false").param("graphType", "Node").param("metaData", "false").get();

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.getBody().asString());
		Assert.assertEquals(code, 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

	@Test(enabled = true, priority = 29)
	public void test_Business_Rule_Builder_Monitors() throws FileNotFoundException {
		test = extent.startTest("Business rule:Builder Monitors");

		RestAssured.baseURI = api_url + "/cdm/classtype/metric/all?";

		Response response = RestAssured.given().header("Accept", "*/*").header("token", superadmin_token)
				.param("addFields", "false").param("metaData", "false").get();

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.getBody().asString());
		Assert.assertEquals(code, 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

	@Test(enabled = true, priority = 30)
	public void test_Get_Node_Count_by_Type() throws FileNotFoundException {
		test = extent.startTest("Get Node count by Type");

		RestAssured.baseURI = api_url + "/vertex/count/type/Sensor";

		response = RestAPIUtility.createGETRequest(RestAssured.baseURI);

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.getBody().asString());
		Assert.assertEquals(code, 200);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

}
