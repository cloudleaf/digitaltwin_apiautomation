package cloudleafApiautomation;

import static org.testng.Assert.assertEquals;

import org.json.simple.parser.ParseException;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import common.APIBasePage;
import common.RestAPIUtility;
import common.Utils;
import io.restassured.RestAssured;

public class DELETERequest extends APIBasePage {

	@Test(enabled = true, priority = 1)
	public void test_Delete_a_node() throws ParseException {

		// start of create node

		RestAssured.baseURI = api_url + "/vertex";

		String nodeID = Utils.createUUID();
		String name = "Trailer-" + Utils.generateRandomNumber();

		response = RestAPIUtility.createNode(RestAssured.baseURI, nodeID, name);

		System.out.println("Response :" + response.asString());
		System.out.println("Status Code :" + response.getStatusCode());

		System.out.println("ID: " + RestAPIUtility.parseJson(response, "id"));

		assertEquals(response.getStatusCode(), 201);
		assertEquals(RestAPIUtility.parseJson(response, "id"), nodeID);
		// end of create node

		// start of delete node
		test = extent.startTest("Delete a node");

		RestAssured.baseURI = api_url + "/vertex/" + nodeID;

		response = RestAPIUtility.createDeleteRequest(RestAssured.baseURI);

		System.out.println("Response :" + response.asString());
		System.out.println("Status Code :" + response.getStatusCode());

		assertEquals(response.getStatusCode(), 200);
		assertEquals("Deleted", response.asString());

		// end of delete node

		// check whether node is deleted

		RestAssured.baseURI = api_url + "/vertex/" + nodeID;
		response = RestAPIUtility.createGETRequest(RestAssured.baseURI);

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.asString());
		System.out.println("Response Body: " + response.asString().length());

		assertEquals(code, 200);
		assertEquals(response.asString().length(), 0);

		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

	@Test(enabled = true, priority = 2)
	public void test_Delete_a_RuleTemplate() {

		test = extent.startTest("Delete a RuleTemplate");

		// start of create rule template
		RestAssured.baseURI = api_url + "/rule/template";

		String name = "RuleTemplate_" + Utils.generateRandomNumber();
		String label = "RuleTemplate_" + Utils.generateRandomNumber() + "_T";
		String nodeID = Utils.createUUID();

		response = RestAPIUtility.createRuleTemplate(RestAssured.baseURI, name, label, nodeID);

		System.out.println("Response :" + response.asString());
		System.out.println("Status Code :" + response.getStatusCode());

		assertEquals(response.getStatusCode(), 200);

		System.out.println("Name: " + RestAPIUtility.parseJson(response, "data.nodeProperties.name"));
		System.out.println("Label: " + RestAPIUtility.parseJson(response, "data.nodeProperties.label"));

		String ID = RestAPIUtility.parseJson(response, "data.nodeProperties.id");
		System.out.println("ID: " + ID);

		assertEquals(RestAPIUtility.parseJson(response, "data.nodeProperties.name"), name);
		assertEquals(RestAPIUtility.parseJson(response, "data.nodeProperties.label"), label);
		// end of create rule template

		RestAssured.baseURI = api_url + "/rule/template/" + ID;

		response = RestAPIUtility.createDeleteRequest(RestAssured.baseURI);

		System.out.println("Response :" + response.asString());
		System.out.println("Status Code :" + response.getStatusCode());

		assertEquals(response.getStatusCode(), 200);
		assertEquals("Deleted", response.asString());

		// check if rule template is deleted

		RestAssured.baseURI = api_url + "/rule/template/" + ID;

		response = RestAPIUtility.createGETRequest(RestAssured.baseURI);

		int code = response.getStatusCode();
		System.out.println("Status Code: " + code);
		System.out.println("Response Body: " + response.asString());

		System.out.println("Nodes: " + RestAPIUtility.getResponseList(response, "nodes"));
		System.out.println("Nodes Len: " + RestAPIUtility.getResponseList(response, "nodes").size());

		assertEquals(response.getStatusCode(), 200);
		assertEquals(RestAPIUtility.getResponseList(response, "nodes").size(), 0);

		test.log(LogStatus.INFO, "Response :" + response.asString());
		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());
	}

	@Test(enabled = true, priority = 3)
	public void test_Delete_a_Rule() {
		test = extent.startTest("Delete a Rule");

		// start of create rule

		RestAssured.baseURI = api_url + "/rule";

		response = RestAPIUtility.createRule(RestAssured.baseURI);
		System.out.println("Response :" + response.asString());
		System.out.println("Status Code :" + response.getStatusCode());

		assertEquals(response.getStatusCode(), 200);

		String ID = RestAPIUtility.parseJson(response, "data.nodeProperties.id");
		System.out.println("ID: " + ID);

		// end of create rule

		RestAssured.baseURI = api_url + "/rule/" + ID;

		response = RestAPIUtility.createDeleteRequest(RestAssured.baseURI);

		System.out.println("Response :" + response.asString());
		System.out.println("Status Code :" + response.getStatusCode());

		assertEquals(response.getStatusCode(), 200);
		assertEquals("Deleted", response.asString());

		// check if rule is deleted
		RestAssured.baseURI = api_url + "/rule/" + ID;
		response = RestAPIUtility.createGETRequest(RestAssured.baseURI);

		System.out.println("Response :" + response.asString());
		System.out.println("Status Code :" + response.getStatusCode());

		assertEquals(response.getStatusCode(), 200);

		jsonPathEvaluator = response.jsonPath();
		
		System.out.println("Nodes Len: " + RestAPIUtility.getResponseList(response, "nodes").size());

		assertEquals(RestAPIUtility.getResponseList(response, "nodes").size(), 0);

		test.log(LogStatus.INFO, "Response :" + response.asString());
		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());
	}

	@Test(enabled = true, priority = 4)
	public void test_Delete_Entity_Type() {

		test = extent.startTest("Delete Entity Type");

		RestAssured.baseURI = api_url + "/cdm/Zone";

		response = RestAPIUtility.createDeleteRequest(RestAssured.baseURI);

		System.out.println("Response :" + response.asString());
		System.out.println("Status Code :" + response.getStatusCode());

		assertEquals(response.getStatusCode(), 401);
		assertEquals("Unauthorized to perform this operation!", response.asString());

		test.log(LogStatus.INFO, "Response :" + response.asString());
		test.log(LogStatus.INFO, "Status Code :" + response.getStatusCode());

	}

}
