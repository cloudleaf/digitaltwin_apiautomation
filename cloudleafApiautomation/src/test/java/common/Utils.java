package common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.Random;
import java.util.UUID;


public class Utils extends APIBasePage {

	static Properties properties = new Properties();
	public static final String CONFIG_FILE = System.getProperty("user.dir") + File.separator + "test.java.resources"
			+ File.separator + "config.properties";

	public static final String ASSET_FILE = System.getProperty("user.dir") + File.separator + "test.java.resources"
			+ File.separator + "importDataFiles" + File.separator + "asset_asset.csv";

	public static final String REPORTS_DIR = System.getProperty("user.dir") + File.separator + "ExtentReports";

	public static String getPropValuesFromConfig(String propertyPath, String propertyKey) throws FileNotFoundException {
		String propFileName = null;

		FileInputStream fileInput = new FileInputStream(propertyPath);

		if (propertyPath != null) {
			try {
				properties.load(fileInput);
			} catch (Exception e) {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}
		}
		return (String) properties.get(propertyKey);
	}

	public static String createUUID() {

		return UUID.randomUUID().toString();
	}

	public static String generateRandomNumber() {
		return String.format("%06d", new Random().nextInt(1000000));
	}

	public static void deleteFolder(File folder) {
		File[] files = folder.listFiles();
		if (files != null) { // some JVMs return null for empty dirs
			for (File f : files) {
				if (f.isDirectory()) {
//					deleteFolder(f);
				} else {
					f.delete();
				}
			}
		}
//		folder.delete();
		System.out.println("Done...");
	}

	public static String calculateTimeDifference(String startDate, String endDate) throws Exception, ParseException {

		Date d1 = null;
		Date d2 = null;
		String timeDiff = "";

		d1 = sdf.parse(startDate);
		d2 = sdf.parse(endDate);

		// in milliseconds
		long diff = d2.getTime() - d1.getTime();

		long diffSeconds = diff / 1000 % 60;
		long diffMinutes = diff / (60 * 1000) % 60;
		long diffHours = diff / (60 * 60 * 1000) % 24;
		long diffDays = diff / (24 * 60 * 60 * 1000);

		if (diffDays != 0) {
			timeDiff = timeDiff + diffDays + " Day(s), ";
		}
		if (diffHours != 0) {
			timeDiff = timeDiff + diffHours + " Hour(s), ";
		}
		if (diffMinutes != 0) {
			timeDiff = timeDiff + diffMinutes + " Minute(s), ";
		}
		if (diffSeconds != 0) {
			timeDiff = timeDiff + diffSeconds + " Second(s).";
		}

		System.out.println(timeDiff);
		return timeDiff;
	}

	public static String getCurrentTimeStamp() {
		Calendar cal = Calendar.getInstance();
		Date date = cal.getTime();

		return sdf.format(date);// returns date in dd-MM-yyyy HH:mm:ss
								// format eg:12-02-2012 12:34:42
	}

	public static void main(String args[]) {

		File f = new File(REPORTS_DIR);

		deleteFolder(f);
//		System.out.println(Utils.createUUID());

	}

}
